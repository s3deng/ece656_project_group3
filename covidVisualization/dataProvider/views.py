from django.http import HttpResponse
from django.core import serializers
from .models import Counties, States
import json

STATE_BY_DATE = {}
INITIAL_STATE = False

def lazy_init():
    global STATE_BY_DATE
    states = States.objects.values()
    states_name = States.objects.values('state').distinct()
    states_name_fips = []
    for state in states_name:
        fips = States.objects.filter(state=state['state']).values('fips').distinct()
        states_name_fips.append({
            'state': state['state'],
            'fips': fips[0]['fips']
        })
    for state in states:
        date_key = state['date'].strftime('%Y-%m-%d')
        if date_key not in STATE_BY_DATE:
                STATE_BY_DATE[date_key] = {}
        state['date'] = state['date'].strftime('%Y-%m-%d')
        STATE_BY_DATE[date_key][int(state['fips'])] = state
    
    for date in STATE_BY_DATE:
        for state in states_name_fips:
            if int(state['fips']) not in STATE_BY_DATE[date]:
                STATE_BY_DATE[date][int(state['fips'])] = {
                    'date': date,
                    'state': state['state'],
                    'fips': state['fips'],
                    'cases': 0,
                    'deaths': 0
                }
    
    INITIAL_STATE = True

    states_name_fips_dict = {}
    for state in states_name_fips:
        states_name_fips_dict[state['state']] = state['fips']

    with open('states_name_fips.json', 'w') as f:
        json.dump(states_name_fips_dict, f)
    return states_name_fips_dict


def get_all_counties(request):
    counties = Counties.objects.values()
    for county in counties:
        county['date'] = county['date'].strftime('%Y-%m-%d')
    return HttpResponse(json.dumps(list(counties)), content_type='application/json')

def get_all_states(request):
    states = States.objects.values()
    for state in states:
        state['date'] = state['date'].strftime('%Y-%m-%d')
    return HttpResponse(json.dumps(list(states)), content_type='application/json')

def get_states_by_date_range(request):
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    states = States.objects.filter(date__range=[start_date, end_date]).values()
    for state in states:
        state['date'] = state['date'].strftime('%Y-%m-%d')
    return HttpResponse(json.dumps(list(states)), content_type='application/json')

def get_counties_by_date_range(request):
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    counties = Counties.objects.filter(date__range=[start_date, end_date]).values()
    for county in counties:
        county['date'] = county['date'].strftime('%Y-%m-%d')
    return HttpResponse(json.dumps(list(counties)), content_type='application/json')

# allow for zero cases
def get_states_by_date_and_state(request):
    if not INITIAL_STATE:
        print('lazy init')
        lazy_init()
    return HttpResponse(json.dumps(STATE_BY_DATE), content_type='application/json')

def add_state_item(request):
    state = json.loads(request.body)
    state = States(
        date=state['date'],
        state=state['state'],
        fips=state['fips'],
        cases=state['cases'],
        deaths=state['deaths']
    )
    state.save()
    return HttpResponse(json.dumps({'status': 'successful'}), content_type='application/json')

def add_county_item(request):
    county = json.loads(request.body)
    county = Counties(
        date=county['date'],
        county=county['county'],
        state=county['state'],
        fips=county['fips'],
        cases=county['cases'],
        deaths=county['deaths']
    )
    county.save()
    return HttpResponse(json.dumps({'status': 'successful'}), content_type='application/json')