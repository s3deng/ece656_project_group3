alter table `us-counties` rename casecounty_raw;
alter table `us-states` rename casestate_raw;
alter table `mask-use-by-county` rename maskusecounty_raw;

-- County table
create table county (
	countyfips char(5) not null,
    county varchar(60) not null,
    state varchar(60) not null,
    primary key(countyfips)
);

insert into county
select distinct fips, county, state
from casecounty_raw;

-- State table
create table state (
	statefips char(2) not null,
    state varchar(60) not null,
    primary key(statefips)
);

insert into state
select distinct fips, state
from casestate_raw;

-- CaseCounty table
create table casecounty (
	countyfips char(5) not null,
    date date not null,
    cases int not null,
    deaths int not null,
    
    primary key(countyfips, date)
);

insert into casecounty
select fips, date, cases, deaths
from casecounty_raw;

-- CaseState table
create table casestate (
	statefips char(2) not null,
    date date not null,
    cases int not null,
    deaths int not null,
    
    primary key(statefips, date)
);

insert into casestate
select fips, date, cases, deaths
from casestate_raw;

-- Facemask table
create table maskcounty (
	countyfips char(5) not null,
    never decimal(5, 3) not null,
    rarely decimal(5, 3) not null,
    sometimes decimal(5, 3) not null,
    frequently decimal(5, 3) not null,
    always decimal(5, 3) not null,
    
    primary key(countyfips)
);

insert into maskcounty
select COUNTYFP, NEVER, RARELY, SOMETIMES, FREQUENTLY, ALWAYS
from maskusecounty_raw;

-- triggers
delimiter @@                      
create trigger countytrigger before insert on casecounty for each row
begin
if not exists (select distinct 1 from County where countyfips = new.countyfips) then
signal sqlstate '45000'
    set message_text = 'Attempt to insert case for a county that does not exist';
end if;
end; @@
delimiter ;

delimiter @@    
create trigger statetrigger before insert on casestate for each row
begin
if not exists (select distinct 1 from state where statefips = new.statefips) then
signal sqlstate '45000'
    set message_text = 'Attempt to insert case for a state that does not exist';
end if;
end; @@
delimiter ;

delimiter @@    
create trigger masktrigger before insert on maskcounty for each row
begin
if not exists (select distinct 1 from State where countyfips = new.countyfips) then
signal sqlstate '45000'
    set message_text = 'Attempt to insert mask info for a county that does not exist';
end if;
end; @@
delimiter ;